import { Stack } from "../src/stack";

describe('When creating a new stack', () => {
    it('should be empty', () => {
        // given
        // when a new stack is created
        const stack = new Stack();

        // then the stack is empty
        expect(stack.isEmpty()).toBe(true);
    });
});

describe('When pushing onto the stack', () => {
    it('should have a single item on the stack when one item is pushed', () => {
        // given an empty stack
        const stack = new Stack();

        // when an item is pushed onto the stack
        stack.push();

        // then a single item should be on the stack
        expect(stack.size()).toBe(1);
    });

    it('should have two items on the stack when two items are pushed', () => {
        // given an empty stack
        const stack = new Stack();

        // when two items are pushed onto the stack
        stack.push();
        stack.push();

        // then two items should be on the stack
        expect(stack.size()).toBe(2);
    });
});

describe('When peeking at the stack', () => {
    it('should retrieve the item last pushed onto the stack and should not be empty when a single item is on the stack', () => {
        // given a stack with a single random item
        const stack = new Stack();
        const randomItem = Math.random();
        stack.push(randomItem);

        // when the stack is peeked at
        const peekedItem = stack.peek();

        // then the item peeked at should be the item pushed onto the stack
        expect(peekedItem).toEqual(randomItem);
        // and the stack should NOT be empty
        expect(stack.isEmpty()).toBe(false);
    });

    it('should throw an error when the stack is empty', () => {
        // given an empty stack
        const stack = new Stack();

        try {
            // when the stack is peeked at
            stack.peek();
            fail('Expected error, but none thrown');
        } catch(e) {
            // then an underflow error should be thrown
            expect(e.message).toEqual("Underflow");
        }
    });
});

describe('When popping from the stack', () => {
    it('should retrieve the item last pushed onto the stack and should remove said item from the stack and be empty when a single item is pushed onto the stack', () => {
        // given a stack with a single random item
        const stack = new Stack();
        const randomItem = Math.random();
        stack.push(randomItem);

        // when the stack is popped
        const poppedItem = stack.pop();

        // then the item popped should be the item pushed
        expect(poppedItem).toEqual(randomItem);
        // and the stack should be empty
        expect(stack.isEmpty()).toBe(true);
    });

    it('should retrieve the item last pushed onto the stack and remove said item, but have an item remaining when two items are pushed onto the stack', () => {
        // given a stack with two items
        const stack = new Stack();
        const firstItem = Math.random();
        stack.push(firstItem);
        const secondItem = Math.random();
        stack.push(secondItem);

        // when the stack is popped
        const poppedItem = stack.pop();

        // then the popped item should be the last item pushed onto the stack
        expect(poppedItem).toEqual(secondItem);
        // and a single item should remain on the stack
        expect(stack.size()).toEqual(1);
        // and that item should be the first item pushed onto the stack
        expect(stack.peek()).toEqual(firstItem);
    });

    it('should throw an error when the stack is empty', () => {
        // given an empty stack
        const stack = new Stack();

        try {
            // when the stack is peeked at
            stack.pop();
            fail('Expected error, but none thrown');
        } catch(e) {
            // then an underflow error should be thrown
            expect(e.message).toEqual("Underflow");
        }
    });
});
