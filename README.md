# Hands on Introduction to Test-Driven Development
## Goal
The goal of this repository is to develop a fully functional Stack based on the specifications presented in `stack.spec.js`.

## Requirements
- Node installed and configured on your machine
  - To install, visit https://nodejs.org
  - Alternatively if you're using a Mac, you can install with Homebrew using the command `brew install node`
- Basic understanding of JavaScript

## Instructions
To properly develop the Stack, focus on one test in particular and develop the minimum amount of code to pass the test. After the test has passed, refactor (if applicable) and then move on to the next test. Keep repeating until all tests have passed.

### First Time Setup
Open a terminal window at the cloned repository's directory and run the command `npm install`.

### Running the Tests
Run the command `npm test`.